import { v4 as uuid } from 'uuid';

export class DestinoViaje {  
   private selected: boolean;
   public servicios: string[];
   id = uuid();
   
   constructor(public nombre: string, public imagenUrl: string, public votes: number = 0) {
      this.servicios = ['pileta','desayuno'];
      // this.selected = false;
   }

   setSelected(s: boolean): void {
      // alert("s="+s);
      // alert("(antes) this.selected="+this.selected);
      // Set.apply(this.selected).s;
      this.selected = s;
      // alert("(despues) this.selected="+this.selected);
   }

   isSelected() {
      return this.selected;
   } 
   
   voteUp() {
      this.votes++;
   } 
   
   voteDown() {
      this.votes--;
   } 
   
}
